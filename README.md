# cicd

## Get Gitlab runner

```python
$ docker pull gitlab/gitlab-runner
```

## Start Gitlab runner

```python
$ docker run -d --name gitlab-runner --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```

### Connect local runner with our Gitlab

```python
$ docker exec -it gitlab-runner gitlab-runner register 
```
- Settings > CI/CD > Runner > expand : to find credentials to connect local runner to repository
- follow instruction (choose docker > debian:latest for system)

### Connect gitlab to azure vm via ssh

- Check in Settings > CI/CD > variables > expand : and add new variable with ssh key as file